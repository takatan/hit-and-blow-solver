extern crate core;

mod game;

use crate::game::{filter_candidates, input_validator, parse_input};
use web_sys::HtmlInputElement;
use yew::prelude::*;

const ANSWER_LENGTH: usize = 8;

#[function_component(AnswerInput)]
pub fn answer_input() -> Html {
    let inputs = use_state(|| {
        (0..ANSWER_LENGTH)
            .map(|_| "".to_string())
            .collect::<Vec<String>>()
    });
    let oninput = {
        let inputs = inputs.clone();
        Callback::from(move |e: InputEvent| {
            let elem: HtmlInputElement = e.target_unchecked_into();
            let name: usize = elem.get_attribute("name").unwrap().parse().unwrap();
            let value = e.data();
            match value {
                Some(value) => {
                    let mut x = (*inputs).clone();
                    x[name] = x[name].clone() + &value;
                    inputs.set(x);
                }
                None => {
                    let mut x = (*inputs).clone();
                    x[name] = "".to_string();
                    inputs.set(x);
                }
            }
        })
    };
    html! {
        <div>
        {
            (0..ANSWER_LENGTH).map(|n| {
                html!{
                <div>{n}
                <input type="text" name={n.to_string()} minlength="6" maxlength="6" size="6" value={(*inputs)[n].clone()} oninput={oninput.clone()}/>
            { &*inputs[n] } {"/"} { input_validator(&*inputs[n])}
                </div>}
        }).collect::<Html>()
        }
        <div>
            {
                inputs.iter().map(|i| input_validator(i)).collect::<Html>()
            }
        </div>
        <hr />
        <div>
            {
                inputs.iter().filter(|i| input_validator(i)).collect::<Html>()
            }
        </div>
        <hr />
         <div>
            {
                filter_candidates(&inputs.iter().filter(|i| input_validator(i)).map(|i| parse_input(i)).collect::<Vec<_>>()).len()
            }
        </div>
         <div>
            {
                filter_candidates(&inputs.iter().filter(|i| input_validator(i)).map(|i| parse_input(i)).collect::<Vec<_>>()).iter().map(|c| html!{
            <div>
                {format!("{:?}", c)}
            </div>
            })
                .collect::<Html>()
            }
        </div>
        </div>
    }
}

fn main() {
    yew::start_app::<AnswerInput>();
}
