use crate::game::Color::{Blue, Green, Magenta, Red, White, Yellow};
use itertools::Itertools;
use regex::Regex;
use std::cmp::min;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Color {
    Red,
    Blue,
    Green,
    Yellow,
    Magenta,
    White,
}

const SIZE: usize = 4;
type Row = [Color; SIZE];

#[derive(Debug)]
pub struct Candidate {
    row: Row,
}

#[derive(Debug, PartialEq)]
pub struct Answer {
    hit: usize,
    blow: usize,
    row: Row,
}

pub const COLORS: [Color; 6] = [Red, Blue, Green, Yellow, Magenta, White];

pub fn build_candidates() -> Vec<Candidate> {
    (0..SIZE)
        .map(|_| COLORS.clone())
        .multi_cartesian_product()
        .map(|e: Vec<Color>| Candidate {
            row: e.try_into().unwrap(),
        })
        .collect::<Vec<Candidate>>()
}

fn count_hit_and_blow(a: &Row, b: &Row) -> (usize, usize) {
    let hit: usize = (0..SIZE).filter(|i| a[*i] == b[*i]).count();
    let blow = COLORS
        .iter()
        .map(|c| {
            min(
                a.iter().filter(|&e| e == c).count(),
                b.iter().filter(|&e| e == c).count(),
            )
        })
        .sum::<usize>()
        - hit;
    (hit, blow)
}

pub fn filter_candidates(answers: &[Answer]) -> Vec<Candidate> {
    let all_candidates = build_candidates();
    answers.iter().fold(all_candidates, |candidates, answer| {
        candidates
            .into_iter()
            .filter(|candidate| {
                count_hit_and_blow(&answer.row, &candidate.row) == (answer.hit, answer.blow)
            })
            .collect()
    })
}

pub fn input_validator(input: &str) -> bool {
    let re = Regex::new(r"[01234][01234][rbgymw]{4}").unwrap();
    re.is_match(input)
}

pub fn parse_input(input: &str) -> Answer {
    if input_validator(input) {
        let mut chars = input.chars();
        Answer {
            hit: chars.next().unwrap().to_digit(10).unwrap() as usize,
            blow: chars.next().unwrap().to_digit(10).unwrap() as usize,
            row: [
                char_to_color(chars.next().unwrap()),
                char_to_color(chars.next().unwrap()),
                char_to_color(chars.next().unwrap()),
                char_to_color(chars.next().unwrap()),
            ],
        }
    } else {
        panic!("Not expected")
    }
}

fn char_to_color(c: char) -> Color {
    match c {
        'r' => Red,
        'b' => Blue,
        'g' => Green,
        'y' => Yellow,
        'm' => Magenta,
        'w' => White,
        _ => panic!("Not expected"),
    }
}
#[cfg(test)]
mod tests {
    use crate::game::Color::{Blue, Green, Magenta, Red, White, Yellow};
    use crate::game::{count_hit_and_blow, filter_candidates, input_validator, Answer};
    use crate::parse_input;

    #[test]
    fn test_count_hit_and_blow() {
        let r = count_hit_and_blow(&[Red, Blue, Red, Yellow], &[Blue, Red, Blue, Red]);
        assert_eq!(r, (0, 3));
    }

    #[test]
    fn test_filter_candidates() {
        let r = filter_candidates(&[Answer {
            hit: 4,
            blow: 0,
            row: [Blue, Blue, Red, Yellow],
        }]);
        assert_eq!(r.len(), 1);
    }

    #[test]
    fn test_filter_candidates2() {
        let r = filter_candidates(&[
            Answer {
                hit: 3,
                blow: 0,
                row: [Blue, Blue, White, White],
            },
            Answer {
                hit: 2,
                blow: 1,
                row: [Blue, Blue, Red, Yellow],
            },
        ]);
        // r.iter().for_each(|e| println!("{:?}", e));
        assert_eq!(r.len(), 2);
    }

    #[test]
    fn test_filter_candidates3() {
        let r = filter_candidates(&[
            Answer {
                hit: 1,
                blow: 1,
                row: [Blue, Red, Green, Yellow],
            },
            Answer {
                hit: 0,
                blow: 3,
                row: [Blue, White, White, Magenta],
            },
            Answer {
                hit: 2,
                blow: 1,
                row: [Magenta, Red, Blue, White],
            },
        ]);
        r.iter().for_each(|e| println!("{:?}", e));
    }

    #[test]
    fn test_input_validator() {
        assert!(!input_validator("0"));
        assert!(!input_validator("00rbg"));
        assert!(input_validator("01rbgw"));
    }

    #[test]
    fn test_parse_input() {
        assert_eq!(
            parse_input("01rrrr"),
            Answer {
                hit: 0,
                blow: 1,
                row: [Red, Red, Red, Red]
            }
        )
    }
}
